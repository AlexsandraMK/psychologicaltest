﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase
{
    class User
    {
        public int User_id;
        public string LastName;
        public string FirstName;
        public string Patronymic;
        public string Login;
        public string Password;
        public DateTime Date_of_birth; // DateTime
        public int Gender; // bool
        public int Group_id; // int
        public int Psycho; // bool
        public string Organization; // if Psycho = True
    }

    public class Table
    {
        public int Table_id;
        public string Table_title;
        public string Table_description;
        public int Table_size;
        public int Usermain_id;
    }

    public class ResponceDB
    {
        public class Autho
        {
            public int Answer; //int 0 - ok, -1 - логина не существует, 1 - проблема с бд
            public int User_id;
        }

        public class Registr
        {
            public int Answer; //int 0 - ok, int 1 - не ок
        }

        public class UserInfo
        {
            public int Answer; //int 0 - ok, int 1 - не ok
            public string LastName;
            public string FirstName;
            public string Patronymic;
            public string Login;
            public DateTime Date_of_birth; // DateTime
            public int Gender; // bool
            public int Group_id; // int
            public int Psycho; // bool
            public string Organization; // if Psycho = True
        }

        public class UserExists
        {
            public int Answer; // 0 - нет, 1 - существует логин, -1 - проблема в бд
        }

        public class AddTable
        {
            public int Answer;
            public int Table_id;
        }

        public class AddCell
        {
            public int Answer;
        }

        public class isMainUser
        {
            public int Answer;
        }

        public class DeleteTable
        {
            public int Answer;
        }

        public class ShowTableById
        {
            public int Answer;
            public Table Table;
        }

        public class ShowTablesforUser //0 - передано, 1 - таблиц нет, -1 - проблема с бд
        {
            public int Answer;
            public List<Table> Tables;
        }

    }
}

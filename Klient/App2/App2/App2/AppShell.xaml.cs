﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App2.Enum;
using Xamarin.Forms;
using App2.Views;
using Xamarin.Forms.Xaml;

namespace App2
{
    public partial class AppShell : Shell
    {
        UserRoles userRoleForTest = UserRoles.Psychologist;

        public AppShell()
        {

            InitializeComponent();

            CreateFlyoutElement(new UserProfilePage());
            CreateFlyoutElement(new CreatingTablePage());
            CreateFlyoutElement(new TestingPage());
            if(userRoleForTest == UserRoles.Psychologist)
                CreateFlyoutElement(new ViewingPatientsPage());
            CreateFlyoutElement(new ViewingOwnTestsPage());

            CreateFlyoutElement(new SettingsPage());
            CreateFlyoutElement(new HelpPage());

        }


        private FlyoutItem CreateFlyoutElement(ContentPage page)
        {
            FlyoutItem item = new FlyoutItem
            {
                Title = page.Title,
                Route = page.ToString().Split('.')[2].ToLower(),
                Items =
                {
                    new Tab
                    {
                        Items = { new ShellContent {Content = page} }
                    }
                }
            };

            this.Items.Add(item);
            return item;
        }
    }
}
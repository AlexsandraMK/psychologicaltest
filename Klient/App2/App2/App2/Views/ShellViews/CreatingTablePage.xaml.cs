﻿using App2.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreatingTablePage : ContentPage
    {

        protected internal ObservableCollection<Table> Tables { get; set; }
        public CreatingTablePage()
        {
            InitializeComponent();
            Tables = new ObservableCollection<Table>
            {
                new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                 new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                  new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                   new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                    new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                     new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                      new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                       new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                        new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                         new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                          new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") },
                           new Table { Name = "Table 1", Size = 5, Comment = "Первая штука", Color=tableFrame.BackgroundColor, TextColor=Color.FromHex("#001F67") }
            };
            
            tableList.BindingContext = Tables;
            Routing.RegisterRoute("edittable", typeof(EditTablePage));
        }

        private async void CreateTableButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EditTablePage(null));
        }

        private int lastInd = -1;

        private void tableList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            
            StackPanelButton.IsVisible = true;
            if (e.SelectedItem != null)
            {
                
                if (lastInd != -1)
                {
                    Tables[lastInd].color = tableFrame.BackgroundColor;
                    Tables[lastInd].TextColor = Color.FromHex("#001F67");
                    Tables[lastInd].OnPropertyChanged();
                }

                int viewInd = e.SelectedItemIndex;
                if (viewInd != -1)
                {
                    Tables[viewInd].color = Color.FromHex("#3E5893");
                    Tables[viewInd].TextColor = Color.FromHex("#FFFFFF");
                    lastInd = viewInd;
                    Tables[lastInd].OnPropertyChanged();
                }
                
            }

        }
    }
}
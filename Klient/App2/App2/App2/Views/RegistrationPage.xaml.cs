﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App2.Enum;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage
    {
        public RegistrationPage(UserRoles Role)
        {
            InitializeComponent();

            if (Role == UserRoles.Psychologist)
                OrganizationStackLayout.IsVisible = true;
            else
                OrganizationStackLayout.IsVisible = false;
        }

        private async void RegistrationButton_ClickedAsync(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("../");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PsychologicalTestClient.Model
{
    public class UserData
    {
        private string login;
        private string role;
        private string lastName;
        private string firstName;
        private string middleName;

        public string Login { get => login; set => login = value; }
        public string Role { get => role; set => role = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string MiddleName { get => middleName; set => middleName = value; }

        public UserData(string login, string role, string lastName, string firstName, string middleName)
        {
            this.login = login;
            this.role = role;
            this.lastName = lastName;
            this.firstName = firstName;
            this.middleName = middleName;
        }

        public UserData()
        {

        }
    }
}

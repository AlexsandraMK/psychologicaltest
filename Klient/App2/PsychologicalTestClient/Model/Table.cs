﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PsychologicalTestClient.Entity
{
    public class Table
    {
        private string name;
        private int side_size;
        private string description;
        private int id;

        private List<Cell> cells;

        public Table()
        {
        }

        public Table(int id, string name, int side_size, string description)
        {
            this.name = name;
            this.side_size = side_size;
            this.description = description;
            this.id = id;
        }

        public string Name { get => name; set => name = value; }
        public int Side_size { get => side_size; set => side_size = value; }
        public string Description { get => description; set => description = value; }
        public int Table_id { get => id; set => id = value; }
        public List<Cell> Cells { get => cells; set => cells = value; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

    public class Cell
    {
        private int value;
        private int color;
        private int id;

        public Cell()
        {
        }

        public Cell(int value, int color, int id)
        {
            this.value = value;
            this.color = color;
            this.id = id;
        }

        public Cell(int value, int color)
        {
            this.value = value;
            this.color = color;
        }

        public int Id { get => id; set => id = value; }
        public int Value { get => value; set => this.value = value; }
        public int Color { get => color; set => color = value; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}

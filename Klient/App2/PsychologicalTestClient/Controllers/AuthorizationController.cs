﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PsychologicalTestClient.ServiceUserRef;

namespace PsychologicalTestClient.Controllers
{
    static class AuthorizationController
    {
        static private SystemUserAPIClient client = new SystemUserAPIClient();

        static public int authorizeUser(string login, string password)
        {

            SystemUser.ResponseUserId resp = client.ToAutorization(login, password);
            if (resp.Status != 200) throw new Exception("Error in authorize");
            Console.WriteLine(resp.Message);
            return resp.UserId;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PsychologicalTestClient.ServiceUserRef;
using PsychologicalTestClient.ServiceTableRef;
using SystemUser;
using PsychologicalTestClient.Entity;

namespace PsychologicalTestClient.Controllers
{
    class EditorTableController
    {
        static private SystemTableAPIClient client = new SystemTableAPIClient();

        static public ObservableCollection<Entity.Table> getUserTables(int userId)
        {
            ResponseListTable resp = client.GetListTableById(userId);
            if (resp.Status != 200) throw new Exception("Error in tables");
            

            ObservableCollection<Entity.Table> Tables = new ObservableCollection<Entity.Table>();
            
            for(int i = 0; i < resp.List.Count; i++)
            {
                Tables.Add(new Entity.Table(resp.List[i].IdTable, resp.List[i].Title, resp.List[i].Size, resp.List[i].Description));
            }
            
            return Tables;
        }


        static public void deleteUserTable(int userId, int tableId)
        {
            ResponseStatus resp = client.ToDeleteTable(userId, tableId);
            if (resp.Status != 200) throw new Exception("Error in delete tables");

            return;
        }

        static public void saveUserTable(int userId, Table table)
        {
            List<RequeCell> cells = new List<RequeCell>();
            foreach(var cell in table.Cells)
            {
                cells.Add(new RequeCell(cell.Value, cell.Color));
            }
            RequeTable requeTable = new RequeTable(table.Name, table.Description, table.Side_size, cells); 
            ResponseStatus resp = client.ToAddTable(userId, requeTable);
            if (resp.Status != 200) throw new Exception("Error in save tables");

            return;
        }
    }
}

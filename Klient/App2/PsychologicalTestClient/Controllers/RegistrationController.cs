﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PsychologicalTestClient.ServiceUserRef;

namespace PsychologicalTestClient.Controllers
{
    public static class RegistrationController
    {
        static private SystemUserAPIClient client = new SystemUserAPIClient();

        public static bool IsPasswordsEqual(string firstPassword, string secondPassword)
        {

            return String.Equals(firstPassword, secondPassword);
        }

        public static void RegistrationUser(string userLastName,
                                            string userFirstName,
                                            string userMiddleName,
                                            string userDateBirth,
                                            int userGender,
                                            string organization,
                                            string login,
                                            string password,
                                            int userRole)
        {
            SystemUser.ResponseStatus resp = client.ToRegistration(new SystemUser.RequeUser(userFirstName, userLastName, userMiddleName, login, password, userDateBirth, userGender, userRole, organization));
            if (resp.Status != 200) throw new Exception("Error in authorize");
            Console.WriteLine(resp.Message);
        }
    }
}

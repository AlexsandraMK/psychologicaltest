﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PsychologicalTestClient.Model;
using PsychologicalTestClient.ServiceUserRef;

namespace PsychologicalTestClient.Controllers
{
    public static class UserProfileController
    {
        static private SystemUserAPIClient client = new SystemUserAPIClient();

        static public UserData getUserData(int userId)
        {
            SystemUser.ResponseUserInfo resp = client.GetUserInfo(userId);
            if (resp.Status != 200) throw new Exception("Error in authorize");

            UserData userData = new UserData(resp.Login, resp.Role.ToString(), resp.SecondName, resp.FirstName, resp.MiddleName);

            return userData;
        }
    }
}

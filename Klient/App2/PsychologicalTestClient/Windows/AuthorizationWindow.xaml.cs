﻿using PsychologicalTestClient.Controllers;
using PsychologicalTestClient.Windows.UnderWindows;
using System;
using System.Windows;

namespace PsychologicalTestClient.Windows
{
    public partial class AuthorizationWindow : Window
    {

        int userIdForTest = -1;

        public AuthorizationWindow()
        {
            InitializeComponent();

            //Routing.RegisterRoute("registration", typeof(RegistrationPage));

        }

        private void RegistrationButton_Clicked(object sender, EventArgs e)
        {

            ChooseRole underChooseRoleWin = new ChooseRole();
            underChooseRoleWin.Owner = this;
            int userRole = -1;
            if (underChooseRoleWin.ShowDialog() == true)
            {
                userRole = underChooseRoleWin.UserRole;
            }
            

            switch (userRole)
            {
                case 0:
                    {
                        Console.WriteLine("Зашел пользователь");
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("Зашел психолог");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Пользователь не выбрал роль");
                        return;
                    }
            }

            Window registrationUserWindow = new RegistrationWindow(userRole);
            registrationUserWindow.Owner = this;
            this.Visibility = Visibility.Hidden;
            registrationUserWindow.Show();

            //Dictionary<string, UserRoles> UsersDictionary = new Dictionary<string, UserRoles>()
            //{
            //    {"Тестируемый пользователь", UserRoles.TestUser},
            //    {"Психолог", UserRoles.Psychologist}
            //};


            //var choosenActionTask = await DisplayActionSheet("Выберите роль", "Отмена", null,
            //    UsersDictionary.FirstOrDefault(x => x.Value == UserRoles.TestUser).Key,
            //    UsersDictionary.FirstOrDefault(x => x.Value == UserRoles.Psychologist).Key);
            //if (UsersDictionary.ContainsKey(choosenActionTask))
            //    await Navigation.PushAsync(new RegistrationPage(UsersDictionary[choosenActionTask]));
        }

        private void LoginButton_Clicked(object sender, EventArgs e)
        {
            int userAuthId = AuthorizationController.authorizeUser(LoginText.Text, PasswordText.Password);

            Window userWindow = new UserProfileWindow(userAuthId);
            userWindow.Owner = this;
            this.Visibility = Visibility.Hidden;
            userWindow.Show();

            //await Shell.Current.GoToAsync(UserProfilePage.RouteLink);
        }
    }
}
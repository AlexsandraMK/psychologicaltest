﻿
using PsychologicalTestClient.Controllers;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Windows;

using System.Windows.Documents;


namespace PsychologicalTestClient.Windows
{
    /// <summary>
    /// Логика взаимодействия для EditorTableWindow.xaml
    /// </summary>
    public partial class EditorTableWindow : Window
    {
        int userId;
        protected internal ObservableCollection<Entity.Table> Tables { get; set; }
        public EditorTableWindow(int userId)
        {
            Tables = EditorTableController.getUserTables(userId);
            this.userId = userId;
            this.DataContext = Tables;

            InitializeComponent();
        }

        private void GoToUserProfile_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "PsychologicalTestClient.Windows.UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToChangerTable_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "PsychologicalTestClient.Windows.EditorTableWindow") return;

            EditorTableWindow window = new EditorTableWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();

        }

        private void GoToTesting_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToShowPatients_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToShowResults_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToSettings_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToInfo_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToAuthorization_Click(object sender, RoutedEventArgs e)
        {
            AuthorizationWindow window = new AuthorizationWindow();

            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToOwner_Click(object sender, RoutedEventArgs e)
        {

            this.Owner.Visibility = Visibility.Visible;
            this.Close();
        }

        private void DeleteTable_Click(object sender, RoutedEventArgs e)
        {
            Entity.Table selectedTable = (Entity.Table) tableList.SelectedItem;

            if (selectedTable == null)
            {
                MessageBox.Show("Для удаления выберите таблицу");
                return;
            }

            EditorTableController.deleteUserTable(userId, selectedTable.Table_id);

            Tables.Remove(selectedTable);
        }

        private void GoToCreateTable_Click(object sender, RoutedEventArgs e)
        {
            CreateTableWindow window = new CreateTableWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.Show();
        }
    }
}

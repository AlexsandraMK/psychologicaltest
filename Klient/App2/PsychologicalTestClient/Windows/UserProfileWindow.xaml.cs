﻿using PsychologicalTestClient.Controllers;
using PsychologicalTestClient.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsychologicalTestClient.Windows
{
    /// <summary>
    /// Логика взаимодействия для UserProfileWindow.xaml
    /// </summary>
    public partial class UserProfileWindow : Window
    {
        int userId;
        public UserProfileWindow(int userId)
        {
            InitializeComponent();
            this.userId = userId;
            UserData userData = UserProfileController.getUserData(userId);

            Nickname.Text = userData.Login;
            Role.Text = userData.Role;
            LastName.Text = userData.LastName;
            FirstName.Text = userData.FirstName;
            MiddleName.Text = userData.MiddleName;

        }

        private void GoToUserProfile_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "PsychologicalTestClient.Windows.UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToChangerTable_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "PsychologicalTestClient.Windows.EditorTableWindow") return;

            EditorTableWindow window = new EditorTableWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();

        }

        private void GoToTesting_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToShowPatients_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToShowResults_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToSettings_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToInfo_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToAuthorization_Click(object sender, RoutedEventArgs e)
        {
            AuthorizationWindow window = new AuthorizationWindow();
            
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToOwner_Click(object sender, RoutedEventArgs e)
        {
            
            this.Owner.Visibility = Visibility.Visible;
            this.Close();
        }
    }
}

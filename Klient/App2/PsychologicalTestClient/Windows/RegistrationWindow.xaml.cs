﻿using PsychologicalTestClient.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsychologicalTestClient.Windows
{
    /// <summary>
    /// Логика взаимодействия для RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        int userGender = -1;
        int userRole = 0;

        public RegistrationWindow(int userRole)
        {
            InitializeComponent();
            this.userRole = userRole;
            if (userRole == 1)
                OrganizationStackLayout.Visibility = Visibility.Visible;
            else
                OrganizationStackLayout.Visibility = Visibility.Collapsed;
        }

        private void RegistrationButton_Clicked(object sender, EventArgs e)
        {
            Error.Text = "";
            string fPassword = password.Password;
            string sPassword = copePassword.Password;

            if (!RegistrationController.IsPasswordsEqual(fPassword, sPassword))
            {
                Error.Text = "Пароли не совпадают.";
                return;
            }

            if (access.IsChecked == false)
            {
                Error.Text = "Пожалуйста дайте согласие на обработку персональных данных.";
                return;
            }

            RegistrationController.RegistrationUser(lastName.Text, firstName.Text, middleName.Text,
                DataPicker.Text, userGender, Organization.Text, login.Text, fPassword, userRole);

            this.Owner.Visibility = Visibility.Visible;
            this.Close();
        }

        private void female_Check(object sender, RoutedEventArgs e)
        {
            MaleCheckBox.IsChecked = false;
            userGender = 0;
        }

        private void male_Check(object sender, RoutedEventArgs e)
        {
            FemaleCheckBox.IsChecked = false;
            userGender = 1;
        }
    }
}

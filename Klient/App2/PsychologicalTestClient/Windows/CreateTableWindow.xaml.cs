﻿using PsychologicalTestClient.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsychologicalTestClient.Windows
{
    /// <summary>
    /// Логика взаимодействия для CreateTableWindow.xaml
    /// </summary>
    public partial class CreateTableWindow : Window
    {
        int userId;
        Entity.Table table;
        public CreateTableWindow(int userId)
        {
            InitializeComponent();

            this.userId = userId;
            table = new Entity.Table();
        }



        private void GoToUserProfile_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "PsychologicalTestClient.Windows.UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToChangerTable_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "PsychologicalTestClient.Windows.EditorTableWindow") return;

            EditorTableWindow window = new EditorTableWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();

        }

        private void GoToTesting_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToShowPatients_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToShowResults_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToSettings_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToInfo_Click(object sender, RoutedEventArgs e)
        {
            if (this.GetType().ToString() == "UserProfileWindow") return;

            UserProfileWindow window = new UserProfileWindow(userId);
            window.Owner = this;
            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToAuthorization_Click(object sender, RoutedEventArgs e)
        {
            AuthorizationWindow window = new AuthorizationWindow();

            this.Visibility = Visibility.Hidden;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Show();
        }

        private void GoToOwner_Click(object sender, RoutedEventArgs e)
        {
            this.Owner.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Open_Close_SettingsTable_Click(object sender, RoutedEventArgs e)
        {
            if (SettingsTablePanel.Visibility == Visibility.Collapsed)
                SettingsTablePanel.Visibility = Visibility.Visible;
            else SettingsTablePanel.Visibility = Visibility.Collapsed;
        }

        private void ReCreateTable_Click(object sender, RoutedEventArgs e)
        {
            table.Name = TitleTable.Text;
            try {
                table.Side_size = int.Parse(SizeTable.Text);
            }
            catch (Exception err)
            {
                MessageBox.Show("Размерность должна быть целым числом");
                return;
            }

            table.Description = DescTable.Text;

            CreateTableView(table.Side_size);

            Open_Close_SettingsTable_Click(sender, e);
        }


        void CreateTableView(int tableSize)
        {
            int sizeCell = 50;
            Table.Children.Clear();
            Table.RowDefinitions.Clear();
            Table.ColumnDefinitions.Clear();
            Table.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            for (int i = 0; i < tableSize; i++)
            {
                Table.RowDefinitions.Add(new RowDefinition { Height = new GridLength(sizeCell) });
                Table.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeCell) });
            }
            Table.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });



            for (int i = 0; i < tableSize; i++)
            {

                for (int j = 0; j < tableSize; j++)
                {
                    Color color;
                    if ((j + 1 + tableSize * i) % 2 == 0) color = Color.FromRgb(181, 16, 15);
                    else color = Colors.Black;

                    Button btn = new Button
                    {
                        Width = sizeCell,
                        Height = sizeCell,
                        Background = new SolidColorBrush(color),
                        BorderThickness = new Thickness(1),
                        BorderBrush = new SolidColorBrush(Colors.White),
                        Content = new TextBlock {
                            FontSize = 18,
                            FontWeight = FontWeights.Bold,
                            Foreground = new SolidColorBrush(Colors.White),
                            Text = "0"
                        },
                        Uid = (j + 1 + tableSize * i).ToString()

                    };

                    btn.Click += ChangeCell_Click;

                    Table.Children.Add(btn);

                    Grid.SetRow(btn, i);
                    Grid.SetColumn(btn, j);

                }

            }

             void ChangeCell_Click(object sender, RoutedEventArgs e)
             {
                Button btn = (Button)sender;

                int cellColor;

                if (((SolidColorBrush)btn.Background).Color == new SolidColorBrush(Colors.Black).Color)
                    cellColor = 1;
                else
                    cellColor = 0;



                int cellValue;
                if (String.IsNullOrEmpty(((TextBlock)btn.Content).Text))
                    cellValue = 0;
                else
                    cellValue = int.Parse(((TextBlock)btn.Content).Text);

                UnderWindows.UnderChangeCellWindow window = new UnderWindows.UnderChangeCellWindow(cellValue, cellColor);
                window.Owner = this;
                if(window.ShowDialog() == true)
                {
                    if (window.CellColor == 0)
                        btn.Background = new SolidColorBrush(Color.FromRgb(181, 16, 15));
                    else
                        btn.Background = new SolidColorBrush(Colors.Black);

                    if (window.CellValue1 > 0)
                        ((TextBlock)btn.Content).Text = window.CellValue1.ToString();

                }


             }

        }

        private void SaveTable_Click(object sender, RoutedEventArgs e)
        {
            table.Cells = new List<Entity.Cell>();

            for (int i = 0; i < table.Side_size; i++)
            {

                for (int j = 0; j < table.Side_size; j++)
                {
                    Button cell = (Button)GetByUid(Table, (j + 1 + table.Side_size * i).ToString());

                    int cellColor;

                    if (((SolidColorBrush)cell.Background).Color == new SolidColorBrush(Colors.Black).Color)
                        cellColor = 1;
                    else
                        cellColor = 0;

                    table.Cells.Add(new Entity.Cell(int.Parse(((TextBlock)cell.Content).Text), cellColor));

                }
            }

            EditorTableController.saveUserTable(userId, table);
        }

        public UIElement GetByUid(DependencyObject rootElement, string uid)
        {
            foreach (UIElement element in LogicalTreeHelper.GetChildren(rootElement).OfType<UIElement>())
            {
                if (element.Uid == uid)
                    return element;
                UIElement resultChildren = GetByUid(element, uid);
                if (resultChildren != null)
                    return resultChildren;
            }
            return null;
        }
    }


}

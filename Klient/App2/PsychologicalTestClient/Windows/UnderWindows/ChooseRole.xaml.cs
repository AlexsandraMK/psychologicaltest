﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsychologicalTestClient.Windows.UnderWindows
{
    /// <summary>
    /// Логика взаимодействия для ChooseRole.xaml
    /// </summary>
    public partial class ChooseRole : Window
    {
        // testUser
        private int userRole = -1;
        public ChooseRole()
        {
          
            InitializeComponent();
        }

        private void ChooseUser_Click(object sender, RoutedEventArgs e)
        {
            userRole = 0;
            this.DialogResult = true;
        }

        private void ChoosePsycholog_Click(object sender, RoutedEventArgs e)
        {
            userRole = 1;
            this.DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public int UserRole
        {
            get => userRole;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsychologicalTestClient.Windows.UnderWindows
{
    /// <summary>
    /// Логика взаимодействия для UnderChangeCellWindow.xaml
    /// </summary>
    public partial class UnderChangeCellWindow : Window
    {

        int cellValue;
        int cellColor;

        public int CellValue1 { get => cellValue; set => cellValue = value; }
        public int CellColor { get => cellColor; set => cellColor = value; }

        public UnderChangeCellWindow(int cellValue, int cellColor)
        {
            InitializeComponent();

            this.cellValue = cellValue;
            this.cellColor = cellColor;

            CellValue.Text = cellValue.ToString();
        }

        private void ChooseRed_Click(object sender, RoutedEventArgs e)
        {
            cellColor = 0;
        }

        private void ChooseBlack_Click(object sender, RoutedEventArgs e)
        {
            cellColor = 1;
        }

        private void Change_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cellValue = int.Parse(CellValue.Text);
            }
            catch
            {
                MessageBox.Show("Число ячейки должно быть целым числом");
            }

            
            this.DialogResult = true;
        }
    }
}

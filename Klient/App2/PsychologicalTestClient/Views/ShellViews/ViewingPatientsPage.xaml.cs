﻿using App2.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace App2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewingPatientsPage : ContentPage
    {

        protected internal ObservableCollection<User> Patients { get; set; }
        public ViewingPatientsPage()
        {
            InitializeComponent();
            Patients = new ObservableCollection<User>
            {
                new User { Name = "Sasha", Date = new DateTime(2000, 5, 16) },
                new User { Name = "Ira", Date = new DateTime(2000, 2, 12) },
                new User { Name = "Nikita", Date =  new DateTime(2000, 10, 5) }
            };

            patientList.BindingContext = Patients;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {

        }

        private void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        }
    }
}
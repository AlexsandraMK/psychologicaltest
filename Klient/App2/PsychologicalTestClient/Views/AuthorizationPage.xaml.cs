﻿using System;
using System.Collections.Generic;
using System.Linq;
using App2.Enum;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace App2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class AuthorizationPage : ContentPage
    {

        int userIdForTest = 1;

        public AuthorizationPage()
        {
            InitializeComponent();
            
            Routing.RegisterRoute("registration", typeof(RegistrationPage));

            TestWCF.Text = "Доступные типы подключений:\n";
            var profiles = Connectivity.ConnectionProfiles;
            foreach (var profile in profiles)
            {
                TestWCF.Text += $"{profile.ToString()} \n";
            }
        }





        private async void RegistrationButton_Clicked(object sender, EventArgs e)
        {
            Dictionary<string, UserRoles> UsersDictionary = new Dictionary<string, UserRoles>()
            {
                {"Тестируемый пользователь", UserRoles.TestUser},
                {"Психолог", UserRoles.Psychologist}
            };
            

            var choosenActionTask = await DisplayActionSheet("Выберите роль", "Отмена", null,
                UsersDictionary.FirstOrDefault(x => x.Value == UserRoles.TestUser).Key,
                UsersDictionary.FirstOrDefault(x => x.Value == UserRoles.Psychologist).Key);
            if (UsersDictionary.ContainsKey(choosenActionTask))
                await Navigation.PushAsync(new RegistrationPage(UsersDictionary[choosenActionTask]));
        }

        private async void LoginButton_Clicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync(UserProfilePage.RouteLink);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace PsychologicalTestClient.Views
{
    public partial class AuthorizationWindow : Window
    {

        int userIdForTest = 1;

        public AuthorizationWindow()
        {
            InitializeComponent();
        }

        public AuthorizationWindow()
        {
            InitializeComponent();
            
            Routing.RegisterRoute("registration", typeof(RegistrationPage));

        }

        private async void RegistrationButton_Clicked(object sender, EventArgs e)
        {
            Dictionary<string, UserRoles> UsersDictionary = new Dictionary<string, UserRoles>()
            {
                {"Тестируемый пользователь", UserRoles.TestUser},
                {"Психолог", UserRoles.Psychologist}
            };
            

            var choosenActionTask = await DisplayActionSheet("Выберите роль", "Отмена", null,
                UsersDictionary.FirstOrDefault(x => x.Value == UserRoles.TestUser).Key,
                UsersDictionary.FirstOrDefault(x => x.Value == UserRoles.Psychologist).Key);
            if (UsersDictionary.ContainsKey(choosenActionTask))
                await Navigation.PushAsync(new RegistrationPage(UsersDictionary[choosenActionTask]));
        }

        private async void LoginButton_Clicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync(UserProfilePage.RouteLink);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
        }
    }
}
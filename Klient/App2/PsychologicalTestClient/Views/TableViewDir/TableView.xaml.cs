﻿using App2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2.Views.TableViewDir
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TableView : ContentPage
    {
        private double sizeCell = 60;
        public TableView(Table table)
        {
            
            InitializeComponent();


            tableGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            for (int i = 0; i < table.Size; i++)
            {
                tableGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(sizeCell) });
                tableGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeCell) });
            }
            tableGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });



            for (int i = 0; i < table.Size; i++)
            {

                for (int j = 0; j < table.Size; j++)
                    tableGrid.Children.Add(new Button
                    {
                        WidthRequest = sizeCell,
                        HeightRequest = sizeCell,
                        BackgroundColor = Color.Red,
                        BorderWidth = 5,
                        BorderColor = Color.Black
                    }, j + 1, i) ;

            }

        }

        public TableView()
        {
            int defaulSizeTable = 5;
            

            InitializeComponent();
            tableGrid.ColumnDefinitions.Add(new ColumnDefinition {Width = new GridLength(1, GridUnitType.Star) });
            for (int i = 0; i < defaulSizeTable; i++)
            {
                tableGrid.RowDefinitions.Add(new RowDefinition {Height = new GridLength(sizeCell) });
                tableGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeCell) });
            }
            tableGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });



            for (int i = 0; i < defaulSizeTable; i++)
            {
                
                for (int j = 0; j < defaulSizeTable; j++)
                    tableGrid.Children.Add(new Button
                    {
                        WidthRequest = sizeCell,
                        HeightRequest = sizeCell,
                        BackgroundColor = Color.Red,
                        BorderWidth = 2,
                        BorderColor = Color.Black
                    }, j + 1, i);

            }


        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;

namespace App2.Models
{
    public class Table : INotifyPropertyChanged
    {
        private int size;
        private string name;
        private string comment;
        public Color color;
        private Color textColor;



        public Table() { }

        public string Name { get => name; set => name = value; }
        public int Size { get => size; set => size = value; }
        public string SizeText => $"{size}x{size}"; 
        public string Comment { get => comment; set => comment = value; }
        public Color Color { get => color; set => color = value; }
        public Color TextColor { get => textColor; set => textColor = value; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}

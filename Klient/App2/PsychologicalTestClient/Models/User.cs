﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace App2.Models
{
   

    public class User : INotifyPropertyChanged
    {
        private string name;

        private DateTime date;

        public User()
        {

        }

        public string Name { get => name; set => name = value; }
        public DateTime Date { get => date; set => date = value; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }


   
}

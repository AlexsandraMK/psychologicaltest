﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFLibrarySasha.RespEntities;

namespace WCFLibrarySasha
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service1" в коде и файле конфигурации.
    public class Service1 : IService1
    {
        public ResponseUserId AuthorizeUser(string login, string password)
        {

            Console.WriteLine(
                String.Format("Авторизация пользователя с логином {0} и паролем {1}.", login, password));

            int userId = 1;



            ResponseUserId response = new ResponseUserId(200, "ok", userId);

            return response;
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }


    }
}

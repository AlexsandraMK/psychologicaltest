﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCFLibrarySasha.RespEntities
{
    public class UserIdResp
    {
        private int userId;

        public UserIdResp(int userId)
        {
            this.userId = userId;
        }

        public int UserId
        {
            get => userId;
        }

    }
}

﻿using System;
using System.ServiceModel;


namespace SystemUser
{
    [ServiceContract]
    public interface ISystemUserAPI
    {
        
        /// <summary>
        /// Принятие запроса на вход пользователя
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        /// <returns>ID пользователя</returns>
        [OperationContract]
        ResponseUserId ToAutorization(string username, string password);


        /// <summary>
        /// Регистрация пользователя
        /// </summary>
        /// <param name="User">Информация о пользователе</param>
        /// <param name="role">Роль пользователя (0 - испытуемый, 1 - психолог)</param>
        /// <returns>ID пользователя</returns>
        [OperationContract]
        ResponseStatus ToRegistration(RequeUser User);

        
        /// <summary>
        /// Получить информацию о пользователе
        /// </summary>
        /// <param name="idUser">ID пользователяе</param>
        /// <returns>Информация о пользователе</returns>
        [OperationContract]
        ResponseUserInfo GetUserInfo(int idUser);
    }
}

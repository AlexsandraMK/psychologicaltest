﻿using System;
using System.ServiceModel;

namespace SystemUser.Service
{
    public interface IServiceUserAPI
    {
        /// <summary>
        /// Поиск ID пользователя с указанным логином и паролем
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        /// <returns>ID пользователя</returns>
        int SignInUser(string username, string password);

        /// <summary>
        /// Добавление пользователя-пациента
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        /// <returns>ID пользователя</returns>
        int AddUser(RequeUser User);

        /*
        /// <summary>
        /// Добавление пользователя-психолога
        /// </summary>
        /// <param name="username">Информация о пользователе</param>
        /// <returns>ID пользователя</returns>
        int AddPsychologist(RequeUser User);
        */

        /// <summary>
        /// Поиск информации о пользователе
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <returns>ID пользователя</returns>
        RequeUser FindUserInfo(int idUser);
    }
}

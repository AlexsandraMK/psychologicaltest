﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using SystemUser;

namespace SystemUser
{
    [ServiceContract]
    public interface ISystemTableAPI
    {
        /// <summary>
        /// Получить список таблиц
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <returns>Список таблиц</returns>
        [OperationContract]
        ResponseListTable GetListTableById(int idUser);

        /// <summary>
        /// Удаление таблицы
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <param name="idTable">ID таблицы</param>
        /// <returns>Список таблиц</returns>
        [OperationContract]
        ResponseStatus ToDeleteTable(int idUser, int idTable);

        /// <summary>
        /// Добавить таблицу
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <param name="idTable">ID таблицы</param>
        /// <returns>Список таблиц</returns>
        [OperationContract]
        ResponseStatus ToAddTable(int idUser, RequeTable table);

        [OperationContract]
        ResponseTableDesc ToFindTableByName(int idUser, string name);
    }
}

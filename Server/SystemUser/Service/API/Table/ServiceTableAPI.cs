﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using SystemUser;

namespace SystemUser.Service
{
    public interface IServiceTableAPI
    {
        List<TableDesc> GetAllTables(int userId);

        void DeleteTable(int idUser, int idTable);

        void AddTable(int idUser, RequeTable table);
    }

}

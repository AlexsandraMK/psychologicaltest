﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using SystemUser;
using SystemUser.Service;

namespace SystemUser
{
    public class SystemTableIMPL : ISystemTableAPI
    {
        /// <summary>
        /// Получить список таблиц
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <returns>Список таблиц</returns>
        public ResponseListTable GetListTableById(int idUser)
        {
            ResponseListTable response;
            try
            {
                IServiceTableAPI service = new ServiceTableIMPL();
                List<TableDesc> desc = service.GetAllTables(idUser);
                response = new ResponseListTable(200, "Ok", desc);
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString() == "404")
                {
                    response = new ResponseListTable(404, "Таблиц нет", null);
                }
                else
                {
                    response = new ResponseListTable(500, "Неизвестная ошибка", null);
                }
            }
            
            
            return response;
        }

        /// <summary>
        /// Удаление таблицы
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <param name="idTable">ID таблицы</param>
        /// <returns>Список таблиц</returns>
        public ResponseStatus ToDeleteTable(int idUser, int idTable)
        {
            ResponseStatus response;
            IServiceTableAPI service = new ServiceTableIMPL();
            try
            {
                service.DeleteTable(idUser, idTable);
                response = new ResponseStatus(200, "Таблица удалена");
                
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString() == "403")
                {
                    response = new ResponseStatus(403, "Отсутствуют права на удаление");
                }
                else
                {
                    response = new ResponseStatus(500, "Неизвестная ошибка");
                }
            }
            return response;
        }

        /// <summary>
        /// Добавление таблицы
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <param name="table">Таблица</param>
        /// <returns>Статус: добавлена/не добавлена</returns>
        public ResponseStatus ToAddTable(int idUser, RequeTable table)
        {
            ResponseStatus response;
            if(table.Size > 10)
            {
                response = new ResponseStatus(400, "Слишком большой размер таблицы");
            }
            else
            {
                if(table.Title == "")
                {
                    response = new ResponseStatus(400, "Некоректное название таблицы");
                }
                else
                {
                    int countRed = 0;
                    int countBlack = 0;
                    foreach (var cell in table.Cells)
                    {
                        if (cell.Color == 0) countRed++; 
                        else countBlack++;
                    }
                    if ((countRed != countBlack + 1) && table.Size % 2 == 1 || countRed != countBlack && table.Size % 2 == 0)
                    {
                        string message;
                        if (table.Size % 2 == 0)
                            message = "Количество красных и чёрных ячеек должно быть одинаковым";
                        else
                            message = "Красных ячеек должно быть на одну больше чёрных";
                        response = new ResponseStatus(400, message);
                    }
                    else
                    {
                        IServiceTableAPI service = new ServiceTableIMPL();
                        try
                        {
                            service.AddTable(idUser, table);
                            response = new ResponseStatus(200, "Таблица сохранена");
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.ToString() == "400")
                            {
                                response = new ResponseStatus(400, "Некорректное значение ячейки");
                            }
                            else
                            {
                                response = new ResponseStatus(500, "Неизвестная ошибка");
                            }
                        }
                    }
                }
                
            }
            return response;
        }

        public ResponseTableDesc ToFindTableByName(int idUser, string name)
        {
            ResponseTableDesc response = null;
            return response;
        }
    }
}

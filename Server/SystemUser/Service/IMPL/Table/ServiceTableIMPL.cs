﻿using DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemUser;
using static DataBase.Program;

namespace SystemUser.Service
{
    public class ServiceTableIMPL : IServiceTableAPI
    {
        public void AddTable(int idUser, RequeTable table)
        {
            int idTable = -1;
            ResponceDB.AddTable respTable = new ResponceDB.AddTable();
            respTable = DBMethods.AddTable(table.Title, table.Description, table.Size, idUser);
            if (respTable.Answer == 0)
            {
                idTable = respTable.Table_id;
                foreach(RequeCell cell in table.Cells)
                {
                    if (cell.Value < 1 || cell.Value > table.Size * table.Size)
                        throw new Exception("400");
                    ResponceDB.AddCell respCell = new ResponceDB.AddCell();
                    respCell = DBMethods.AddCell(idTable, cell.Color, cell.Value);
                    if (respCell.Answer != 0)
                        throw new Exception("500");
                }
                
            }
            else
            {
                throw new Exception("500");
            }
        }

        public List<TableDesc> GetAllTables(int userId)
        {
            List<TableDesc> list = new List<TableDesc>();
            ResponceDB.ShowTablesforUser resp = new ResponceDB.ShowTablesforUser();
            resp = DBMethods.ShowTablesforUser(userId);
            if (resp.Answer == 0)
            {
                foreach(var table in resp.Tables)
                {
                    list.Add(new TableDesc(table.Table_id, table.Table_title, table.Table_size, table.Table_description));
                }
            }
            else
            {
                if(resp.Answer == 1)
                {
                    
                }
                else
                {
                    throw new Exception("500");
                }
            }
            return list;
        }

        public void DeleteTable(int idUser, int idTable)
        {
            ResponceDB.isMainUser respUserHas = new ResponceDB.isMainUser();
            respUserHas = DBMethods.isMainUser(idTable, idUser);
            if (respUserHas.Answer == 0)
            {
                ResponceDB.DeleteTable respDel = new ResponceDB.DeleteTable();
                respDel = DBMethods.DeleteTable(idTable);
                if (respDel.Answer != 0)
                    throw new Exception("500");
            }
            else
            {
                if(respUserHas.Answer == 1)
                    throw new Exception("403");
                else
                    throw new Exception("500");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBase;
using static DataBase.Program;

namespace SystemUser.Service
{
    class ServiceUserIMPL : IServiceUserAPI
    {
        /// <summary>
        /// Поиск ID пользователя с указанным логином и паролем
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        /// <returns>ID пользователя</returns>
        public int SignInUser(string username, string password)
        {
            int idUser = -1;
            ResponceDB.Autho resp = new ResponceDB.Autho();
            resp = DBMethods.Autho(username, password);
            switch(resp.Answer)
            {
                case -1:
                    throw new Exception("404");
                    break;
                case 1:
                    throw new Exception("500");
                    break;
                case 0:
                    idUser = resp.User_id;
                    break;
            }
            return idUser;
        }


        /// <summary>
        /// Добавление пользователя-пациента в базу данных
        /// </summary>
        /// <param name="User">Информация о пользователе-пациенте</param>
        /// <returns>ID пользователя</returns>
        public int AddUser(RequeUser User)
        {
            int status = 1;
            ResponceDB.UserExists respExists = new ResponceDB.UserExists();
            respExists = DBMethods.UserExists(User.login);
            if (respExists.Answer == 0)
            {
                ResponceDB.Registr resp = new ResponceDB.Registr();
                resp = DBMethods.Registr(User.secondName, User.firstName, User.middleName, User.Login, User.password, User.dateOfBirth, User.gender,
                    User.role, User.organization);
                if(resp.Answer != 0)
                {
                    throw new Exception("500");
                }
                else
                {
                    status = resp.Answer;
                }
            }
            else
            {
                if (respExists.Answer == -1)
                {
                    throw new Exception("500");
                }

                else
                {
                    throw new Exception("403");
                }
            }
            return status;
        }
        
        /// <summary>
        /// Поиск информации о пользователе
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <returns>ID пользователя</returns>
        public RequeUser FindUserInfo(int idUser)
        {
            //return new RequeUser("Никита", "Бортников", "Евгеньевич", "antohachekhov", null, "2000.05.10", 1, 1, 1, "НАТО");
            RequeUser info;
            ResponceDB.UserInfo resp = new ResponceDB.UserInfo();
            resp = DBMethods.UserInfo(idUser);       
            if (resp.Answer != 0)
                throw new Exception("500");
            else
            {
                info = new RequeUser(resp.FirstName, resp.LastName, resp.Patronymic, resp.Login, null,
                    resp.Date_of_birth.ToString("d", CultureInfo.GetCultureInfo("de-DE")), resp.Gender, 
                    resp.Psycho, resp.Group_id, resp.Organization);
            }
            return info;
        }
    }
}

﻿using System;
using SystemUser.Service;

namespace SystemUser
{
    public class SystemUserIMPL : ISystemUserAPI
    {
        /// <summary>
        /// Принятие запроса на вход пользователя
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        /// <returns>ID пользователя</returns>
        public ResponseUserId ToAutorization(string username, string password)
        {
            IServiceUserAPI service = new ServiceUserIMPL();

            int id_user;
            ResponseUserId response_id;

            if(username == "" || password == "")
            {
                response_id = new ResponseUserId(400, "Неверные входные данных", -1);
            }
            else
            {
                try
                {
                    id_user = service.SignInUser(username, password);
                    response_id = new ResponseUserId(200, "Ok", id_user);
                }
                catch (Exception ex)
                {
                    if (ex.Message.ToString() == "404")
                    {
                        response_id = new ResponseUserId(404, "Пользователь не найден", -1);
                    }
                    else
                    {
                        response_id = new ResponseUserId(500, "Неизвестная ошибка", -1);
                    }

                }
            }
            return response_id;
        }

        /// <summary>
        /// Регистрация пользователя
        /// </summary>
        /// <param name="User">Информация о пользователе</param>
        /// <param name="role">Роль пользователя (0 - испытуемый, 1 - психолог)</param>
        /// <returns>ID пользователя</returns>
        public ResponseStatus ToRegistration(RequeUser User)
        {
            ResponseStatus response;
            int status = 1;
            IServiceUserAPI service = new ServiceUserIMPL();
            try
            {
                status = service.AddUser(User);
                response = new ResponseStatus(200, "Учётная запись создана");
                
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString() == "403")
                {
                    response = new ResponseStatus(403, "Пользователь с таким логином уже существует");
                }
                else
                {
                    response = new ResponseStatus(500, "Неизвестная ошибка");
                }
            }            
            return response;
        }
        
        /// <summary>
        /// Получить информацию о пользователе
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <returns>Информация о пользователе</returns>
        public ResponseUserInfo GetUserInfo(int idUser)
        {
            ResponseUserInfo responseUserInfo;
            IServiceUserAPI service = new ServiceUserIMPL();
            try
            {
                RequeUser info = service.FindUserInfo(idUser);
                responseUserInfo = new ResponseUserInfo(200, "Пользователь найден", info);   
            }
            catch (Exception)
            {
                responseUserInfo = new ResponseUserInfo(500, "Неизвестная ошибка");
            }
            return responseUserInfo;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBase;
using MySql.Data.MySqlClient;

namespace DataBase
{
    public class Program
    {
        const string sql = "Database=Gorbov;Data Source=127.0.0.1;User Id=root;Password=Irinka_2000";
        //127.0.0.1 - локальный
        //26.80.16.145 - радмин

        static void Main(string[] args)
        {

            string db = "Gorbov";
            string localhost = "127.0.0.1";
            string user = "root";
            string pass = "Irinka_2000";

            /*ResponceDB.Autho resp = new ResponceDB.Autho();
            resp = DBMethods.Autho("nikita", "123");
            Console.WriteLine("resp окончательный = " + resp.Answer + " " + resp.User_id);*/

            /*ResponceDB.Registr resp = new ResponceDB.Registr();
            resp = DBMethods.Registr("Ефименко", "Александра", "Сергеевна", "Paprike", "123", "16.05.2000" , 0, 0, null);
            Console.WriteLine("resp окончательный = " + resp.Answer);*/

            /*ResponceDB.UserInfo resp = new ResponceDB.UserInfo();
            resp = DBMethods.UserInfo(2);
            Console.WriteLine("resp окончательный = " + resp.Answer + " " + resp.LastName);*/

            /*ResponceDB.UserExists resp = new ResponceDB.UserExists();
            resp = DBMethods.UserExists("SunriseDagger1");
            Console.WriteLine("resp окончательный = " + resp.Answer);*/

            /*ResponceDB.AddTable resp = new ResponceDB.AddTable();
            resp = DBMethods.AddTable("Таблица2", "Описание таблицы2", 5, 1);
            Console.WriteLine("resp окончательный = " + resp.Answer + " " + resp.Table_id);*/

            /*int TI = 2;
            int[] Tcolor = new[] { 0, 1, 0 };
            int[] Tvalue = new[] { 10, 5, 20 };
            for (int i = 0; i < 3; i++)
            {
                ResponceDB.AddCell resp = new ResponceDB.AddCell();
                resp = DBMethods.AddCell(TI, Tcolor[i], Tvalue[i]);
                Console.WriteLine("resp окончательный = " + resp.Answer + "\n");
            }*/

            /*ResponceDB.isMainUser resp = new ResponceDB.isMainUser();
            resp = DBMethods.isMainUser(1, 1);
            Console.WriteLine("resp окончательный = " + resp.Answer);*/

            /*ResponceDB.DeleteTable resp = new ResponceDB.DeleteTable();
            resp = DBMethods.DeleteTable(2);
            Console.WriteLine("resp окончательный = " + resp.Answer);*/

            /*ResponceDB.ShowTablesforUser resp = new ResponceDB.ShowTablesforUser();
            resp = DBMethods.ShowTablesforUser(1);
            Console.WriteLine("resp окончательный = " + resp.Answer);
            for(int i = 0; i < resp.Tables.Count; i++)
            {
                Console.WriteLine("Table " + resp.Tables[i].Table_id + " = " + resp.Tables[i].Table_id + " " + resp.Tables[i].Table_title 
                    + " " + resp.Tables[i].Table_description + " " + resp.Tables[i].Table_size + 
                    " Usermain_id = " + resp.Tables[i].Usermain_id);
            }*/

            /*ResponceDB.ShowTableById resp = new ResponceDB.ShowTableById();
            resp = DBMethods.ShowTableById(3);
            Console.WriteLine("resp окончательный = " + resp.Answer);
            Console.WriteLine("Table " + resp.Table.Table_id + " = " + resp.Table.Table_id + " " + resp.Table.Table_title
                    + " " + resp.Table.Table_description + " " + resp.Table.Table_size +
                    " Usermain_id = " + resp.Table.Usermain_id);*/

            Console.ReadLine();
        }

        public static class DBMethods
        {
            public static ResponceDB.Autho Autho(string login, string password)
            {
                User us = new User()
                {
                    Login = login,
                    Password = password
                };

                ResponceDB.Autho resp = new ResponceDB.Autho();
                resp = AuthoFunc(sql, us);
                Console.WriteLine(resp.Answer + " " + resp.User_id);
                return resp;
            }

            public static ResponceDB.Registr Registr(string lastname, string firstname, string patronymic, string login, string password,
                string dateS, int gender, int psycho, string organization)
            {
                string[] dateSS = dateS.Split('.');
                int dateDay = Convert.ToInt32(dateSS[0]);
                int dateMonth = Convert.ToInt32(dateSS[1]);
                int dateYear = Convert.ToInt32(dateSS[2]);
                DateTime date = new DateTime(dateYear, dateMonth, dateDay);

                User us = new User()
                {
                    LastName = lastname,
                    FirstName = firstname,
                    Patronymic = patronymic,
                    Login = login,
                    Password = password,
                    Date_of_birth = date,
                    Gender = gender,
                    Psycho = psycho,
                    Organization = organization
                };

                ResponceDB.Registr resp = new ResponceDB.Registr();
                resp = RegUserFunc(sql, us);
                Console.WriteLine(resp.Answer + " ");
                return resp;
            }

            public static ResponceDB.UserInfo UserInfo(int UserId)
            {
                ResponceDB.UserInfo resp = new ResponceDB.UserInfo();
                resp = UserFunc(sql, UserId);
                Console.WriteLine(resp.Answer + " " + resp.LastName);
                return resp;
            }

            public static ResponceDB.UserExists UserExists(string login)
            {
                ResponceDB.UserExists resp = new ResponceDB.UserExists();
                resp = UserExistsFunc(sql, login);
                Console.WriteLine(resp.Answer);
                return resp;
            }

            public static ResponceDB.AddTable AddTable(string title, string description, int size, int UserId)
            {
                ResponceDB.AddTable resp = new ResponceDB.AddTable();
                resp = AddTableFunc(sql, title, description, size, UserId);
                Console.WriteLine(resp.Answer + " " + resp.Table_id);
                return resp;
            }

            public static ResponceDB.AddCell AddCell(int TableId, int color, int value)
            {
                ResponceDB.AddCell resp = new ResponceDB.AddCell();
                resp = AddCellFunc(sql, TableId, color, value);
                Console.WriteLine(resp.Answer);
                return resp;
            }

            public static ResponceDB.isMainUser isMainUser(int TableId, int UsermainId)
            {
                ResponceDB.isMainUser resp = new ResponceDB.isMainUser();
                resp = isMainUserFunc(sql, TableId, UsermainId);
                Console.WriteLine(resp.Answer);
                return resp;
            }

            public static ResponceDB.DeleteTable DeleteTable(int TableId)
            {
                ResponceDB.DeleteTable resp = new ResponceDB.DeleteTable();
                resp = DeleteTableFunc(sql, TableId);
                Console.WriteLine(resp.Answer);
                return resp;
            }

            public static ResponceDB.ShowTableById ShowTableById(int TableId)
            {
                ResponceDB.ShowTableById resp = new ResponceDB.ShowTableById();
                resp = ShowTableByIdFunc(sql, TableId);
                Console.WriteLine(resp.Answer);
                return resp;
            }

            public static ResponceDB.ShowTablesforUser ShowTablesforUser(int UserId)
            {
                ResponceDB.ShowTablesforUser resp = new ResponceDB.ShowTablesforUser();
                resp = ShowTablesforUserFunc(sql, UserId);
                Console.WriteLine(resp.Answer);
                return resp;
            }

        }

        static ResponceDB.Autho AuthoFunc(string sql, User us)
        {
            MySqlData.MySqlExecute.MyResult result = new MySqlData.MySqlExecute.MyResult();

            string quary = "select Password from Users where Login = @Login";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);
            MySqlParameter ParamLogin = new MySqlParameter("@Login", MySqlDbType.VarChar);
            commRC.Parameters.Add(ParamLogin).Value = us.Login;
            ResponceDB.Autho responce = new ResponceDB.Autho();

            result = MySqlData.MySqlExecute.SqlScalar(connRC, commRC);
            Console.WriteLine("проверка пароль = " + result.ResultText.ToString());

            if (result.HasError == false)
            {
                if (result.ResultText == null)
                {
                    Console.WriteLine("Пользователь с таким логином не найден");
                    responce.Answer = -1;
                }
                else if (us.Password == result.ResultText.ToString())
                {
                    MySqlData.MySqlExecute.MyResult result1 = new MySqlData.MySqlExecute.MyResult();
                    string quary1 = "select User_id from Users where Login = @Login1";
                    MySqlConnection connRC1 = new MySqlConnection(sql);
                    MySqlCommand commRC1 = new MySqlCommand(quary1, connRC1);
                    MySqlParameter ParamLogin1 = new MySqlParameter("@Login1", MySqlDbType.VarChar);
                    commRC1.Parameters.Add(ParamLogin1).Value = us.Login;
                    result1 = MySqlData.MySqlExecute.SqlScalar(connRC1, commRC1);
                    responce.Answer = 0;
                    responce.User_id = Convert.ToInt32(result1.ResultText);
                    Console.WriteLine(responce.Answer + " - " + responce.User_id);
                }
            }
            else
            {
                Console.WriteLine("It's error: " + result.ErrorText);
                responce.Answer = 1;
            }

            return responce;
        }

        static ResponceDB.UserExists UserExistsFunc(string sql, string login)
        {
            MySqlData.MySqlExecute.MyResult result = new MySqlData.MySqlExecute.MyResult();

            string quary = "select EXISTS(select * from Users where Login = @Login)";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);
            MySqlParameter ParamLogin = new MySqlParameter("@Login", MySqlDbType.VarChar);
            commRC.Parameters.Add(ParamLogin).Value = login;
            ResponceDB.UserExists responce = new ResponceDB.UserExists();

            if (result.HasError == false)
            {
                result = MySqlData.MySqlExecute.SqlScalar(connRC, commRC);
                if(Convert.ToInt32(result.ResultText) == 1) responce.Answer = 1;
                else responce.Answer = 0;
                Console.WriteLine("проверка на существование логина = " + result.ResultText.ToString());
            }
            else
            {
                Console.WriteLine("It's error: " + result.ErrorText);
                responce.Answer = -1;
            }

            return responce;
        }
        static ResponceDB.UserInfo UserFunc(string sql, int UserId)
        {
            string quary = "select * from Users where User_id = @User_id";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);

            MySqlData.MySqlExecuteData.MyResultData result = new MySqlData.MySqlExecuteData.MyResultData();

            MySqlParameter ParamUserId = new MySqlParameter("@User_id", MySqlDbType.Int32);
            commRC.Parameters.Add(ParamUserId).Value = UserId;

            ResponceDB.UserInfo responce = new ResponceDB.UserInfo();

            result = MySqlData.MySqlExecuteData.SqlReturnDataset(connRC, commRC);

            if (result.HasError == false)
            {
                if (result.ResultData.DefaultView.Table.Rows.Count == 0)
                {
                    Console.WriteLine("Error = null objects found");
                    responce.Answer = 1;
                }
                else
                {
                    responce.LastName = result.ResultData.DefaultView.Table.Rows[0]["LastName"].ToString();
                    responce.FirstName = result.ResultData.DefaultView.Table.Rows[0]["FirstName"].ToString();
                    responce.Patronymic = result.ResultData.DefaultView.Table.Rows[0]["Patronymic"].ToString();
                    responce.Login = result.ResultData.DefaultView.Table.Rows[0]["Login"].ToString();
                    responce.Date_of_birth = Convert.ToDateTime(result.ResultData.DefaultView.Table.Rows[0]["Date_of_birth"]);
                    responce.Gender = Convert.ToInt32(result.ResultData.DefaultView.Table.Rows[0]["Gender"]);
                    responce.Group_id = Convert.ToInt32(result.ResultData.DefaultView.Table.Rows[0]["Group_id"]);
                    responce.Psycho = Convert.ToInt32(result.ResultData.DefaultView.Table.Rows[0]["Psycho"]);

                    Console.WriteLine(responce.LastName + " " + responce.FirstName + " " + responce.Patronymic + " " + responce.Login + " " +
                        responce.Date_of_birth.ToString("d") + " " + responce.Gender + " " + responce.Group_id + " " + responce.Psycho);

                    responce.Answer = 0;
                }
            }
            else
            {
                Console.WriteLine("It's error: " + result.ErrorText);
                responce.Answer = 2;
            }

            return responce;
        }

        static ResponceDB.Registr RegUserFunc(string sql, User us)
        {
            int Group_id = Group(us.Date_of_birth);

            string quary = "insert Users(LastName, FirstName, Patronymic, Login, Password, Date_of_birth, Group_id," +
                "Gender, Psycho) values (@LastName, @FirstName, @Patronymic, @Login, @Password," +
                "@Date_of_birth, @Group_id, @Gender, @Psycho);";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);
            ResponceDB.Registr responce = new ResponceDB.Registr();

            MySqlData.MySqlExecute.MyResult result = new MySqlData.MySqlExecute.MyResult();

            MySqlParameter ParamLN = new MySqlParameter("@LastName", MySqlDbType.VarChar);
            MySqlParameter ParamFN = new MySqlParameter("@FirstName", MySqlDbType.VarChar);
            MySqlParameter ParamPath = new MySqlParameter("@Patronymic", MySqlDbType.VarChar);
            MySqlParameter ParamLogin = new MySqlParameter("@Login", MySqlDbType.VarChar);
            MySqlParameter ParamPass = new MySqlParameter("@Password", MySqlDbType.VarChar);
            MySqlParameter ParamDate = new MySqlParameter("@Date_of_birth", MySqlDbType.DateTime);
            MySqlParameter ParamGroup = new MySqlParameter("@Group_id", MySqlDbType.Int32);
            MySqlParameter ParamGender = new MySqlParameter("@Gender", MySqlDbType.Int32);
            MySqlParameter ParamPsycho = new MySqlParameter("@Psycho", MySqlDbType.Int32);

            commRC.Parameters.Add(ParamLN).Value = us.LastName;
            commRC.Parameters.Add(ParamFN).Value = us.FirstName;
            commRC.Parameters.Add(ParamPath).Value = us.Patronymic;
            commRC.Parameters.Add(ParamLogin).Value = us.Login;
            commRC.Parameters.Add(ParamPass).Value = us.Password;
            commRC.Parameters.Add(ParamDate).Value = us.Date_of_birth;
            commRC.Parameters.Add(ParamGroup).Value = Group_id;
            commRC.Parameters.Add(ParamGender).Value = us.Gender;
            commRC.Parameters.Add(ParamPsycho).Value = us.Psycho;

            result = MySqlData.MySqlExecute.SqlNonQuery(connRC, commRC);

            if (result.HasError == false)
            {
                Console.WriteLine("Добавлено");
                responce.Answer = 0;
                if (us.Psycho == 1)
                {
                    MySqlData.MySqlExecute.MyResult result1 = new MySqlData.MySqlExecute.MyResult();
                    string quary1 = "select User_id from Users where Login = @Login1";
                    MySqlConnection connRC1 = new MySqlConnection(sql);
                    MySqlCommand commRC1 = new MySqlCommand(quary1, connRC1);
                    commRC1.Parameters.Add(ParamLogin).Value = us.Login;
                    result1 = MySqlData.MySqlExecute.SqlScalar(connRC1, commRC1);

                    int User_id = Convert.ToInt32(result1.ResultText);

                    MySqlData.MySqlExecute.MyResult result2 = new MySqlData.MySqlExecute.MyResult();
                    string quary2 = "insert Psychologists(User_id, Organization) values (@User_Id, @Organization)";
                    MySqlConnection connRC2 = new MySqlConnection(sql);
                    MySqlCommand commRC2 = new MySqlCommand(quary2, connRC2);
                    MySqlParameter ParamUserId = new MySqlParameter("@User_Id", MySqlDbType.Int32);
                    MySqlParameter ParamOrg = new MySqlParameter("@Organization", MySqlDbType.VarChar);
                    commRC2.Parameters.Add(ParamUserId).Value = User_id;
                    commRC2.Parameters.Add(ParamOrg).Value = us.Organization;
                    result2 = MySqlData.MySqlExecute.SqlNonQuery(connRC2, commRC2);

                    responce.Answer = 0;
                }
                
            }
            else
            {
                Console.WriteLine("It's error: " + result.ErrorText);
                responce.Answer = 1;
            }

            return responce;
        }

        static ResponceDB.AddTable AddTableFunc(string sql, string title, string description, int size, int UserId)
        {
            string quary = "insert Tables(Table_title, Table_description, Table_size, Usermain_id)" +
                "values (@Table_title, @Table_description, @Table_size, @Usermain_id)";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);
            ResponceDB.AddTable responce = new ResponceDB.AddTable();

            MySqlData.MySqlExecute.MyResult result = new MySqlData.MySqlExecute.MyResult();

            MySqlParameter ParamTitle = new MySqlParameter("@Table_title", MySqlDbType.VarChar);
            MySqlParameter ParamDesc = new MySqlParameter("@Table_description", MySqlDbType.VarChar);
            MySqlParameter ParamSize = new MySqlParameter("@Table_size", MySqlDbType.Int32);
            MySqlParameter ParamUserId = new MySqlParameter("@Usermain_id", MySqlDbType.Int32);

            commRC.Parameters.Add(ParamTitle).Value = title;
            commRC.Parameters.Add(ParamDesc).Value = description;
            commRC.Parameters.Add(ParamSize).Value = size;
            commRC.Parameters.Add(ParamUserId).Value = UserId;

            result = MySqlData.MySqlExecute.SqlNonQuery(connRC, commRC);

            if (result.HasError == false)
            {
                Console.WriteLine("Добавлено");
                responce.Answer = 0;

                MySqlData.MySqlExecute.MyResult result1 = new MySqlData.MySqlExecute.MyResult();
                string quary1 = "select Table_id from Tables where (Table_title = @Table_title and Usermain_id = @Usermain_id)";
                MySqlConnection connRC1 = new MySqlConnection(sql);
                MySqlCommand commRC1 = new MySqlCommand(quary1, connRC1);
                commRC1.Parameters.Add(ParamTitle).Value = title;
                commRC1.Parameters.Add(ParamUserId).Value = UserId;
                result1 = MySqlData.MySqlExecute.SqlScalar(connRC1, commRC1);

                int Table_id = Convert.ToInt32(result1.ResultText);

                string quary2 = "insert AccessTable(Table_id, User_id) values (@Table_id, @Usermain_id)";
                MySqlConnection connRC2 = new MySqlConnection(sql);
                MySqlCommand commRC2 = new MySqlCommand(quary2, connRC2);

                MySqlData.MySqlExecute.MyResult result2 = new MySqlData.MySqlExecute.MyResult();

                MySqlParameter ParamTableId = new MySqlParameter("@Table_id", MySqlDbType.Int32);

                commRC2.Parameters.Add(ParamTableId).Value = Table_id;
                commRC2.Parameters.Add(ParamUserId).Value = UserId;

                result2 = MySqlData.MySqlExecute.SqlNonQuery(connRC2, commRC2);

                responce.Answer = 0;
                responce.Table_id = Table_id;
            }
            else
            {
                Console.WriteLine("It's error: " + result.ErrorText);
                responce.Answer = 1;
            }

            return responce;
        }

        static ResponceDB.AddCell AddCellFunc(string sql, int TableId, int color, int value)
        {
            string quary = "insert Cells(Table_id, Cell_color, Cell_value) values (@Table_id, @Cell_color, @Cell_value)";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);
            ResponceDB.AddCell responce = new ResponceDB.AddCell();

            MySqlData.MySqlExecute.MyResult result = new MySqlData.MySqlExecute.MyResult();

            MySqlParameter ParamTableID = new MySqlParameter("@Table_id", MySqlDbType.Int32);
            MySqlParameter ParamColor = new MySqlParameter("@Cell_color", MySqlDbType.Int32);
            MySqlParameter ParamValue = new MySqlParameter("@Cell_value", MySqlDbType.Int32);

            commRC.Parameters.Add(ParamTableID).Value = TableId;
            commRC.Parameters.Add(ParamColor).Value = color;
            commRC.Parameters.Add(ParamValue).Value = value;

            result = MySqlData.MySqlExecute.SqlNonQuery(connRC, commRC);

            if (result.HasError == false)
            {
                Console.WriteLine("Добавлено");
                responce.Answer = 0;
            }
            else
            {
                Console.WriteLine("It's error: " + result.ErrorText);
                responce.Answer = 1;
            }

            return responce;
        }

        static ResponceDB.isMainUser isMainUserFunc(string sql, int TableId, int UsermainId)
        {
            MySqlData.MySqlExecute.MyResult result = new MySqlData.MySqlExecute.MyResult();

            string quary = "select Usermain_id from Tables where Table_id = @Table_id";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);
            MySqlParameter ParamTableId = new MySqlParameter("@Table_id", MySqlDbType.Int32);
            commRC.Parameters.Add(ParamTableId).Value = TableId;
            ResponceDB.isMainUser responce = new ResponceDB.isMainUser();

            result = MySqlData.MySqlExecute.SqlScalar(connRC, commRC);
            Console.WriteLine("проверка Usermain = " + result.ResultText.ToString());

            if (result.HasError == false)
            {
                if (Convert.ToInt32(result.ResultText) == UsermainId) responce.Answer = 0;
                else responce.Answer = 1;
            }
            else
            {
                Console.WriteLine("It's error: " + result.ErrorText);
                responce.Answer = -1;
            }

            return responce;
        }
        
        static ResponceDB.DeleteTable DeleteTableFunc(string sql, int TableId)
        {
            string quary = "delete from Cells where Table_id = @Table_id; delete from Tables where Table_id = @Table_id";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);
            ResponceDB.DeleteTable responce = new ResponceDB.DeleteTable();

            MySqlData.MySqlExecute.MyResult result = new MySqlData.MySqlExecute.MyResult();

            MySqlParameter ParamTableId = new MySqlParameter("@Table_id", MySqlDbType.Int32);

            commRC.Parameters.Add(ParamTableId).Value = TableId;

            result = MySqlData.MySqlExecute.SqlNonQuery(connRC, commRC);

            if (result.HasError == false)
            {
                Console.WriteLine("Удалено");
                responce.Answer = 0;
            }
            else
            {
                Console.WriteLine("It's error: " + result.ErrorText);
                responce.Answer = 1;
            }

            return responce;
        }

        static ResponceDB.ShowTableById ShowTableByIdFunc(string sql, int TableId)
        {
            string quary = "select * from Tables where Table_id = @Table_id";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);

            MySqlData.MySqlExecuteData.MyResultData result = new MySqlData.MySqlExecuteData.MyResultData();

            MySqlParameter ParamTableId = new MySqlParameter("@Table_id", MySqlDbType.Int32);
            commRC.Parameters.Add(ParamTableId).Value = TableId;

            result = MySqlData.MySqlExecuteData.SqlReturnDataset(connRC, commRC);

            ResponceDB.ShowTableById responce = new ResponceDB.ShowTableById();

            if (result.HasError == false)
            {
                Table table = new Table()
                {
                    Table_id = TableId,
                    Table_title = result.ResultData.DefaultView.Table.Rows[0]["Table_title"].ToString(),
                    Table_description = result.ResultData.DefaultView.Table.Rows[0]["Table_description"].ToString(),
                    Table_size = Convert.ToInt32(result.ResultData.DefaultView.Table.Rows[0]["Table_size"]),
                    Usermain_id = Convert.ToInt32(result.ResultData.DefaultView.Table.Rows[0]["Usermain_id"])
                };

                responce.Table = table;
            }
            else
            {
                responce.Answer = 1;
            }
               
            return responce;
        }

        static ResponceDB.ShowTablesforUser ShowTablesforUserFunc(string sql, int UserId)
        {
            string quary = "select Table_id from AccessTable where User_id = @User_id";
            MySqlConnection connRC = new MySqlConnection(sql);
            MySqlCommand commRC = new MySqlCommand(quary, connRC);

            MySqlData.MySqlExecuteData.MyResultData result = new MySqlData.MySqlExecuteData.MyResultData();

            MySqlParameter ParamUserId = new MySqlParameter("@User_id", MySqlDbType.Int32);
            commRC.Parameters.Add(ParamUserId).Value = UserId;

            ResponceDB.ShowTablesforUser responce = new ResponceDB.ShowTablesforUser();

            result = MySqlData.MySqlExecuteData.SqlReturnDataset(connRC, commRC);

            List<Table> Tables = new List<Table>();

            if (result.HasError == false)
            {
                if (result.ResultData.DefaultView.Table.Rows.Count == 0)
                {
                    Console.WriteLine("Error = null objects found");
                    responce.Answer = 1;
                }
                else
                {
                    int count = result.ResultData.DefaultView.Table.Rows.Count;
                    Console.WriteLine("count = " + count);
                    for(int i = 0; i < count; i++)
                    {
                        int id = Convert.ToInt32(result.ResultData.DefaultView.Table.Rows[i]["Table_id"]);
                        Console.WriteLine(id);

                        Table table = new Table();

                        table = ShowTableByIdFunc(sql, id).Table;

                        Tables.Add(table);
                    }

                    responce.Tables = Tables;
                    responce.Answer = 0;
                }
            }
            else
            {
                Console.WriteLine("It's error: " + result.ErrorText);
                responce.Answer = -1;
            }

            return responce;
        }

        static int Group(DateTime bday)
        {
            int group = 0;
            DateTime now = DateTime.Today;
            int age = now.Year - bday.Year;
            if (bday > now.AddYears(-age)) age--;

            if (0 <= age && age <= 5) group = 1;
            else if (6 <= age && age <= 15) group = 2;
            else if (16 <= age && age <= 30) group = 3;
            else if (31 <= age && age <= 45) group = 4;
            else if (46 <= age && age <= 60) group = 5;
            else if (61 <= age && age <= 75) group = 6;
            else group = 7;

            return group;
        }
    }
}

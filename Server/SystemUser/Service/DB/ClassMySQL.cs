﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data;
using System.Data;
using MySql.Data.MySqlClient;

namespace DataBase
{
    // Набор компонент для простой работы с MySQL базой данных
    class MySqlData
    {
        // Методы реализующие выполнение запросов с возвращением одного параметра либо без параметров вовсе
        public class MySqlExecute
        {
            // Возвращаемый набор данных
            public class MyResult
            {
                public string ResultText; // Возращает результат запроса
                public bool HasError; // Возвращает True, если произошла ошибка
                public string ErrorText; // Возвращает текст ошибки
            }

            // Запрос к бд с возвращением одного параметра1
            public static MyResult SqlScalar(MySqlConnection connRC, MySqlCommand commRC)
            {
                MyResult result = new MyResult();
                try
                {
                    connRC.Open();
                    try
                    {
                        result.ResultText = commRC.ExecuteScalar().ToString();
                        result.HasError = false;
                    }
                    catch (Exception ex)
                    {
                        result.ErrorText = ex.Message;
                        Console.WriteLine("1 = " + ex.Message.ToString());
                        result.HasError = true;
                    }
                    connRC.Close();
                }
                catch (Exception ex) // Если отсутствует соединение с сервером
                {
                    result.ErrorText = ex.Message;
                    Console.WriteLine("2 = " + ex.Message.ToString());
                    result.HasError = true;
                }
                return result;
            }

            // Выполнение запросов к бд без параметров
            // sql - текст запроса
            // connection - строка подключения
            // Возвращает True, если ошибка, False - успешно
            public static MyResult SqlNonQuery(MySqlConnection connRC, MySqlCommand commRC)
            {
                MyResult result = new MyResult();

                try
                {
                    connRC.Open(); // открытие соединения с бд

                    try
                    {
                        commRC.ExecuteNonQuery();
                        result.HasError = false;
                    }
                    catch (Exception ex)
                    {
                        result.ErrorText = ex.Message;
                        Console.WriteLine(ex.ToString());
                        result.HasError = true;
                    }

                    connRC.Close();
                }
                catch (Exception ex) // Если отсутствует соединение с сервером
                {
                    result.ErrorText = ex.Message;
                    result.HasError = true;
                }

                return result;
            }
        }

        public class MySqlExecuteData
        {
            // Возвращаемый набор данных
            public class MyResultData
            {
                public DataTable ResultData; // Возращает результат запроса
                public bool HasError; // Возвращает True, если произошла ошибка
                public string ErrorText; // Возвращает текст ошибки
            }

            public static MyResultData SqlReturnDataset(MySqlConnection connRC, MySqlCommand commRC)
            {
                MyResultData result = new MyResultData();

                try
                {
                    connRC.Open(); // открытые соединения с бд

                    try
                    {
                        MySqlDataAdapter AdapterP = new MySqlDataAdapter();
                        AdapterP.SelectCommand = commRC;
                        DataSet ds1 = new DataSet();
                        AdapterP.Fill(ds1);
                        result.ResultData = ds1.Tables[0];
                    }
                    catch (Exception ex)
                    {
                        result.ErrorText = ex.Message;
                        result.HasError = true;
                    }

                    connRC.Close();
                }
                catch (Exception ex) // Если отсутствует соединение с сервером
                {
                    result.ErrorText = ex.Message;
                    result.HasError = true;
                }

                return result;
            }
        }
    }
}

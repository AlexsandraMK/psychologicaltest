﻿using DataBase;
using MySqlX.XDevAPI.Relational;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SystemUser
{
    [DataContract]
    public class RequeUser
    {
        public string login;
        public string password;
        public string firstName;
        public string secondName;
        public string middleName;
        public string dateOfBirth;
        public int gender;
        public int role;
        public int group_id;
        public string organization;

        [DataMember]
        public string FirstName
        {
            get => firstName;
            set => firstName = value;
        }

        [DataMember]
        public string SecondName
        {
            get => secondName;
            set => secondName = value;
        }

        [DataMember]
        public string MiddleName
        {
            get => middleName;
            set => middleName = value;
        }

        [DataMember]
        public string Login
        {
            get => login;
            set => login = value;
        }

        [DataMember]
        public string Password
        {
            get => password;
            set => password = value;
        }

        [DataMember]
        public string DateOfBirth
        {
            get => dateOfBirth;
            set => dateOfBirth = value;
        }

        [DataMember]
        public int Gended
        {
            get => gender;
            set => gender = value;
        }

        [DataMember]
        public string Organization
        {
            get => organization;
            set => organization = value;
        }

        [DataMember]
        public int Role
        {
            get => role; set => role = value;
        }

        [DataMember]
        public int GroupId
        {
            get => group_id; set => group_id = value;
        }

        public RequeUser(string firstName, 
            string secondName, 
            string middleName, 
            string login, 
            string password, 
            string dateOfBirth, 
            int gender,
            int role,
            int groupId,
            string organization)
        {
            this.firstName = firstName;
            this.secondName = secondName;
            this.middleName = middleName;
            this.login = login;
            this.password = password;
            this.dateOfBirth = dateOfBirth;
            this.gender = gender;
            this.role = role;
            this.group_id = groupId;
            this.organization = organization;
        }

        public RequeUser(string firstName,
            string secondName,
            string middleName,
            string login,
            string password,
            string dateOfBirth,
            int gender,
            int role,
            string organization)
        {
            this.firstName = firstName;
            this.secondName = secondName;
            this.middleName = middleName;
            this.login = login;
            this.password = password;
            this.dateOfBirth = dateOfBirth;
            this.gender = gender;
            this.group_id = -1;
            this.role = role;
            this.organization = organization;
        }
    }
}

namespace SystemUser
{
    [DataContract]
    public class RequeCell
    {
        int value;
        int color; //0 - красный, 1 - чёрный

        [DataMember]
        public int Value
        {
            get => value;
            set => this.value = value;
        }

        [DataMember]
        public int Color
        {
            get => color;
            set => color = value;
        }

        public RequeCell(int value, int color)
        {
            this.value = value;
            this.color = color;
        }
    }

    [DataContract]
    public class RequeTable
    {
        string title;
        string description;
        int size;
        List<RequeCell> cells;

        [DataMember]
        public string Title
        {
            get => title;
            set => title = value;
        }

        [DataMember]
        public string Description
        {
            get => description;
            set => description = value;
        }

        [DataMember]
        public int Size
        {
            get => size;
            set => size = value;
        }

        [DataMember]
        public List<RequeCell> Cells
        {
            get=> cells;
            set => cells = value;
        }

        public RequeTable( string title, string description, int size, List<RequeCell> cells)
        {
            this.title = title;
            this.description = description;
            this.size = size;
            this.cells = cells;
        }
    }
}

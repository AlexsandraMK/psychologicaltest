﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace SystemUser
{
    [DataContract]
    public class User
    {
        string login;
        string firstName;
        string secondName;
        string middleName;
        string dateOfBirth;
        int gender;
        string organization;
        int role;
        int groupId;

        [DataMember]
        public string Login
        {
            get => login; set => login = value;
        }

        [DataMember]
        public string FirstName
        {
            get => firstName; set => firstName = value;
        }

        [DataMember]
        public string SecondName
        {
            get => secondName; set => secondName = value;
        }

        [DataMember]
        public string MiddleName
        {
            get => middleName; set => middleName = value;
        }

        [DataMember]
        public string DateOfBirth
        {
            get => dateOfBirth;
            set => dateOfBirth = value;
        }

        [DataMember]
        public int Gender
        {
            get => gender; set => gender = value;
        }

        [DataMember]
        public string Organization
        {
            get => organization; set => organization = value;
        }

        [DataMember]
        public int Role
        {
            get => role; set => role = value;
        }

        [DataMember]
        public int GroupId
        {
            get => groupId; set => groupId = value;
        }

        public User(int status, string message, string login, string firstName, string secondName, string middleName, string dateOfBirth,
            int gender, string organization, int role, int groupid)
        {
            this.login = login;
            this.firstName = firstName;
            this.secondName = secondName;
            this.middleName = middleName;
            this.dateOfBirth = dateOfBirth;
            this.gender = gender;
            this.organization = organization;
            this.role = role;
            this.groupId = groupid;
        }
    }
}

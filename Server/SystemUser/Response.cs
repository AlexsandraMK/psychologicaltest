﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SystemUser
{
    [DataContract]
    public class ResponseUserId
    {
        int status;
        string message;
        int userId;

        [DataMember]
        public int Status
        {
            get => status;
            set => status = value;
        }

        [DataMember]
        public string Message
        {
            get => message;
            set => message = value;
        }

        [DataMember]
        public int UserId
        {
            get => userId;
            set => userId = value;
        }

        public ResponseUserId(int status, string message, int userId)
        {
            this.status = status;
            this.message = message;
            this.userId = userId;
        }
    }

    [DataContract]
    public class ResponseUserInfo
    {
        int status;
        string message;
        
        string login;
        string firstName;
        string secondName;
        string middleName;
        string dateOfBirth;
        int gender;
        string organization;
        int role;
        int groupId;

        [DataMember] 
        public int Status
        {
            get=> status;
            set => status = value;
        }

        [DataMember]
        public string Message
        {
            get => message;
            set => message = value;
        }

        [DataMember]
        public string Login
        {
            get => login; set => login = value;
        }

        [DataMember]
        public string FirstName
        {
            get=> firstName; set => firstName = value;
        }

        [DataMember]
        public string SecondName
        {
            get=> secondName; set => secondName = value;
        }

        [DataMember]
        public string MiddleName
        {
            get=> middleName; set => middleName = value;
        }

        [DataMember]
        public string DateOfBirth
        {
            get => dateOfBirth;
            set => dateOfBirth = value;
        }

        [DataMember]
        public int Gender
        {
            get => gender; set => gender = value; 
        }

        [DataMember]
        public string Organization
        {
            get => organization; set => organization = value;
        }

        [DataMember]
        public int Role
        {
            get => role; set => role = value;
        }

        [DataMember]
        public int GroupId
        {
            get => groupId; set => groupId = value;
        }

        public ResponseUserInfo(int status, string message, string login, string firstName, string secondName, string middleName, string dateOfBirth,
            int gender, string organization, int role, int groupid)
        {
            this.status = status;
            this.message = message;

            this.login = login;
            this.firstName = firstName;
            this.secondName = secondName;
            this.middleName = middleName;
            this.dateOfBirth = dateOfBirth;
            this.gender = gender;
            this.organization = organization;
            this.role = role;
            this.groupId = groupid;
        }

        public ResponseUserInfo(int status, string message)
        {
            this.status = status;
            this.message = message;

            this.login = null;
            this.firstName = null;
            this.secondName = null;
            this.middleName = null;
            this.dateOfBirth = null;
            this.gender = 0;
            this.organization = null;
            this.role = 0;
            this.groupId = 0;
        }

        public ResponseUserInfo(int status, string message, RequeUser User)
        {
            this.status = status;
            this.message = message;

            this.login = User.login;
            this.firstName = User.firstName;
            this.secondName = User.secondName;
            this.middleName = User.middleName;
            this.dateOfBirth = User.DateOfBirth;
            this.gender = User.gender;
            this.organization = User.organization;
            this.role = User.role;
            this.groupId = User.group_id;
        }

    }

    [DataContract]
    public class ResponseStatus
    {
        int status;
        string message;

        [DataMember]
        public int Status
        {
            get => status;
            set => status = value;
        }

        [DataMember]
        public string Message
        {
            get => message;
            set => message = value;
        }

        public ResponseStatus(int status, string message)
        {
            this.status = status;
            this.message = message;
        }
    }
}

namespace SystemUser
{
    [DataContract]
    public class ResponseListTable
    {
        int status;
        string message;
        List<TableDesc> list;

        [DataMember]
        public int Status
        {
            get => status;
            set => status = value;
        }

        [DataMember]
        public string Message
        {
            get => message;
            set => message = value;
        }

        [DataMember]
        public List<TableDesc> List
        {
            get => list;
            set => list = value;
        }

        public ResponseListTable(int status, string message, List<TableDesc> list)
        {
            this.status = status;
            this.message = message;
            this.list = list;
        }
    }

    [DataContract]
    public class ResponseTableDesc
    {
        int status;
        string message;
        TableDesc tabledesc;

        [DataMember]
        public int Status
        {
            get => status;
            set => status = value;
        }

        [DataMember]
        public string Message
        {
            get => message;
            set => message = value;
        }

        [DataMember]
        public TableDesc TableDesc
        {
            get => tabledesc;
            set => tabledesc = value;
        }


        public ResponseTableDesc(int status, string message, TableDesc tabledesc)
        {
            this.status = status;
            this.message = message;
            this.tabledesc = tabledesc;
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SystemUser
{
    [DataContract]
    public class TableDesc
    {
        int idTable;
        string title;
        int size;
        string description;

        [DataMember] 
        public int IdTable
        {
            get => idTable;
            set => idTable = value;
        }

        [DataMember]
        public string Title
        {
            get => title;
            set => title = value;
        }

        [DataMember]
        public string Description
        {
            get => description;
            set => description = value;
        }

        [DataMember]
        public int Size
        {
            get => size;
            set => size = value;
        }

        public TableDesc(int idTable, string title, int size, string description)
        {
            this.idTable = idTable;
            this.title = title;
            this.size = size;
            this.description = description;
        }
    }
}
